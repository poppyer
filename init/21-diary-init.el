(setq org-agenda-include-diary t)


(defun find-today()
  (interactive)
  (search-backward (format-time-string date-format-string) nil t)
  (search-forward (format-time-string date-format-string) nil t)
  )

(add-hook 'diary-mode-hook (lambda()
							 (define-key diary-mode-map "\M-." 'find-today)
							 ))


(setq view-diary-entries-initially t
	  mark-diary-entries-in-calendar t
	  number-of-diary-entries 7)
(add-hook 'diary-display-hook 'fancy-diary-display)

;;Here is some code to get rid of the ugly equal signs under the date:

(add-hook 'fancy-diary-display-mode-hook
		  '(lambda ()
			 (alt-clean-equal-signs)))
(defun alt-clean-equal-signs ()
  "This function makes lines of = signs invisible."
  (goto-char (point-min))
  (let ((state buffer-read-only))
	(when state (setq buffer-read-only nil))
	(while (not (eobp))
	  (search-forward-regexp "^=+$" nil 'move)
	  (add-text-properties (match-beginning 0) 
						   (match-end 0) 
						   '(invisible t)))
	(when state (setq buffer-read-only t))))


(set 'today-visible-calendar-hook 'calendar-star-date)
;;(add-hook 'today-visible-calendar-hook 'calendar-mark-today)

  (custom-set-faces
   '(diary ((((class color) (background dark)) (:background "cyan"))))
;;   '(calendar-today ((((class color) (background dark)) (:background "green"))))
   )



(setq appt-display-duration 360000)
(appt-activate t)