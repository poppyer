
;;{{{  speedbar within frame
(require 'speedbar)
(speedbar-change-initial-expansion-list "files")
(global-set-key "\M-s" 'speedbar-update-contents)
(defvar my-speedbar-buffer-name 
  (if (buffer-live-p speedbar-buffer)
      (buffer-name speedbar-buffer)
    "*SpeedBar*"))
(defun my-speedbar-no-separate-frame ()
  (interactive)
  (when (not (buffer-live-p speedbar-buffer))
    (setq speedbar-buffer (get-buffer-create my-speedbar-buffer-name)
          speedbar-frame (selected-frame)
          dframe-attached-frame (selected-frame)
          speedbar-select-frame-method 'attached
          speedbar-verbosity-level 0
          speedbar-last-selected-file nil)
    (set-buffer speedbar-buffer)
    (speedbar-mode)
    (speedbar-reconfigure-keymaps)
    (local-set-key " " 'speedbar-toggle-line-expansion)		;; by poppyer , for ERC speedbar
    (local-set-key "c" 'speedbar-change-initial-expansion-list)	;; by poppyer
    (speedbar-update-contents)
    (speedbar-set-timer 1)
    (make-local-hook 'kill-buffer-hook)
    (add-hook 'kill-buffer-hook
              (lambda () (when (eq (current-buffer) speedbar-buffer)
                           (setq speedbar-frame nil
                                 dframe-attached-frame nil
                                 speedbar-buffer nil)
                           (speedbar-set-timer nil)))))
  (if (get-buffer-window my-speedbar-buffer-name)		;; by poppyer
      (if (string= (buffer-name (current-buffer)) my-speedbar-buffer-name)  ;; by poppyer3
	  (other-window 1)
	(pop-to-buffer my-speedbar-buffer-name)
	)
    (if (one-window-p t)					;; by poppyer2
	(split-window-horizontally 25)
      )
    (set-window-buffer (selected-window)
		       (get-buffer my-speedbar-buffer-name))
    )
  (speedbar-enable-update)		;; turn on auto update
  )
(global-set-key [f8] 'my-speedbar-no-separate-frame)

(defun my-speedbar-visit-file ()
  (other-window 1)
   )

(setq speedbar-before-visiting-file-hook 'my-speedbar-visit-file)
(setq speedbar-before-visiting-tag-hook '(push-mark my-speedbar-visit-file))
;;}}}
