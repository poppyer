(require 'bm)

;;;;;;;;;;;;;;;;;;;;;;;bookmark support;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq bm-restore-repository-on-load t)
(setq bm-show-format-string "%5d %-5s %s")

(global-set-key (kbd "<M-f3>") 'bm-toggle)
(global-set-key (kbd "<f3>")   'bm-next)	;; <f3> is used by kmacro-start-macro-or-insert-counter
(global-set-key (kbd "<C-f3>") 'bm-show)

(global-set-key (kbd "<f5>") 'kmacro-start-macro-or-insert-counter)
(global-set-key (kbd "<C-f2>") 'imenu)
 
;; make bookmarks persistent as default
(setq-default bm-buffer-persistence t)
 
;; Loading the repository from file when on start up.
(add-hook 'after-init-hook 'bm-repository-load)
 
;; Restoring bookmarks when on file find.
(add-hook 'find-file-hooks 'bm-buffer-restore)
 
;; Saving bookmark data on killing a buffer
(add-hook 'kill-buffer-hook 'bm-buffer-save)
 
;; Saving the repository to file when on exit.
;; kill-buffer-hook is not called when emacs is killed, so we
;; must save all bookmarks first.
(add-hook 'kill-emacs-hook '(lambda nil
                              (bm-buffer-save-all)
                              (bm-repository-save)))


;;;;;;;;;;;;;;;;;;;;;;;bookmark end;;;;;;;;;;;;;;;;;;;;;;;;;;;
