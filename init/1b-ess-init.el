(add-to-list 'load-path "~/.emacs.d/ess/lisp")
(require 'ess-site)
(setq ess-ask-for-ess-directory nil)
(setq ess-directory "~/R/")



(add-hook 'ess-mode-hook (lambda()
						   (local-set-key (kbd "TAB") (lambda()
														(interactive)
														(if (looking-at "\\>")
															(ess-list-object-completions)
														  (ess-indent-line)
														  )
														)
										  )
						   ))

(add-hook 'inferior-ess-mode-hook (lambda()
						   (local-set-key (kbd "M-p") 'comint-previous-matching-input-from-input)
						   (local-set-key (kbd "M-n") 'comint-next-matching-input-from-input)
						   ))


;(require 'r-utils)