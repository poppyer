(add-to-list 'load-path "~/.emacs.d/emms/")
(require 'emms-setup)
(emms-standard)
(emms-default-players)


;; need taglib binary package
;; (require 'emms-info-libtag)
;; (setq emms-info-functions '(emms-info-libtag))


;;need mp3info binary
(require 'emms-i18n)
(setq emms-i18n-coding-system-for-read 'gb2312)
;; emms-i18n will automatically do it
;; (setq emms-info-mp3info-coding-system 'gb2312)

(require 'emms-lyrics)
(setq emms-lyrics-coding-system 'gb2312)
(setq emms-lyrics-scroll-p nil)

;; Show the current track each time EMMS
;; starts to play a track with "NP : "
;;(add-hook 'emms-player-started-hook 'emms-show)
;; (setq emms-show-format "NP: %s")
        
;; When asked for emms-play-directory,
;; always start from this one 
(setq emms-source-file-default-directory "~/Music/")
        
;; Want to use alsa with mpg321 ? 
;; (setq emms-player-mpg321-parameters '("-o" "alsa"))
      