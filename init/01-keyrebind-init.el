
(defun set-mark-command-dwim()
  (interactive)
  (if mark-active
	  (progn
		(pop-to-mark-command)
		(pop-to-mark-command)
		)
	(set-mark-command nil))
  )
	
(global-set-key (kbd "C-2") 'set-mark-command-dwim)
(global-set-key (kbd "C-@") 'set-mark-command-dwim)

(defun write-file-region-dwim(filename)
  (interactive "FWrite file: " :title0 "Write File")
  (if mark-active
          (write-region (region-beginning) (region-end) filename nil nil nil t)
        (write-file filename t))
  )
(global-set-key "\C-x\C-w" 'write-file-region-dwim)

(global-set-key (kbd "C-4") 'toggle-input-method)

(global-set-key "\C-xk" 'kill-this-buffer)
(global-set-key "\C-x\C-f" 'find-file-at-point)

(global-set-key "\M-m" (lambda()
			 (interactive)
			 (back-to-indentation)
			 (gnus-horizontal-recenter)
			 )
		)

(load "gnus-sum")
(global-set-key "\C-l" 'gnus-recenter)


(defun mark-line()
  "mark current line"
  (interactive)
  (push-mark)
  (move-end-of-line nil)
  (push-mark nil nil t)
  (move-beginning-of-line nil)
  )

(global-set-key "\M-L" 'mark-line)


(defun poppyer-mark-paragraph()
  (interactive)
  (if (or (not mark-active)
		  (> (mark) (point)))
	  (mark-paragraph nil t)
	(exchange-point-and-mark)
	(mark-paragraph nil t)
	(exchange-point-and-mark)
	)
  )
(global-set-key "\M-h" 'poppyer-mark-paragraph)


(defun poppyer-comment-dwim(&optional n)
  "add function to comment one line"
  (interactive "p")
  (if (or mark-active
		  (not (looking-at "^")))   
	  (comment-dwim nil)
	(apply 'comment-or-uncomment-region
		   (if (> n 0)
			   (list (line-beginning-position) (line-end-position n))
			 (list (line-beginning-position n) (line-end-position))))
	)
  )
(global-set-key "\M-;" 'poppyer-comment-dwim)

(defun next-line-and-indent()
  (interactive)
  (next-line)
  (indent-for-tab-command)
  )


(global-set-key "\C-\M-j" (lambda()
			 (interactive)
			 (move-end-of-line nil)
			 (if (looking-at "\n\n")
			     (next-line-and-indent)
			   (newline-and-indent)
			   )
			 )
		)


;; (defun mark-current-word ()
;;   "Put point at beginning of current word, set mark at end."
;;   (interactive)
;;   (let* ((opoint (point))
;; 	 (word (current-word))
;; 	 (word-length (length word)))
;;     (if (save-excursion
;; 	  ;; Avoid signaling error when moving beyond buffer.
;; 	  (if (> (point-min)  (- (point) word-length))
;; 	      (beginning-of-buffer)
;; 	    (forward-char (- (length word))))
;; 	  (search-forward word (+ opoint (length word))
;; 			  'noerror))
;; 	(progn (push-mark)		; by poppyer, such that can use C-x C-SPACE to restore position
;; 		   (push-mark (match-end 0) nil t)
;; 	       (goto-char (match-beginning 0)))
;;       (error "No word at point" word))))
(defun mark-current-word ()
  "Put point at beginning of current word, set mark at end."
  (interactive)
  (unless (or (looking-at "\\<")
			  (not (current-word t)))
	(backward-word)
	)
  (mark-word nil t)
  )
  
(global-set-key "\M-@" 'mark-current-word)

(global-set-key "\C-w" (lambda()
			     (interactive)
			       (if (string-match "XEmacs" (emacs-version))
				   (kill-region (point) (mark))
				 (if mark-active
				     (kill-region (point) (mark))
				   (server-edit)
				   )
				 )
			       )
		)


(defun exchange-point-and-mark-dwim()
  (interactive)
  (if mark-active
      (exchange-point-and-mark)
    (cons
     (exchange-point-and-mark)
     (deactivate-mark)
     )
    )
  )
(global-set-key "\C-x\C-x" 'exchange-point-and-mark-dwim)


(defun toggle-maximize-window()
  (interactive)
  (if (one-window-p t)
      (list
       (point-to-register ?t)
       (jump-to-register ?w)
       (register-to-point ?t)
       )
    (window-configuration-to-register ?w)
    (delete-other-windows)
    )
  )
(defun toggle-maximize-other-window ()
  (interactive)
  (if (one-window-p t)
      (list
       (jump-to-register ?w)
       )
    (window-configuration-to-register ?w)
    (other-window 1)
    (delete-other-windows)
    )
  )
(global-set-key "\C-x1" 'toggle-maximize-window)
(global-set-key [f7] 'toggle-maximize-window)
(global-set-key [C-f7] 'toggle-maximize-other-window)


(defun split-window-horizontally-and-switch ()
  (interactive)
  (select-window (split-window-horizontally))
  )
(global-set-key "\C-x3" 'split-window-horizontally-and-switch)


(defadvice kill-ring-save (before slickcopy activate compile)
  "When called interactively with no active region, copy a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (list (line-beginning-position) 
	      (line-beginning-position 2)))))
(defadvice kill-region (before slickcut activate compile)
  "When called interactively with no active region, kill a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (list (line-beginning-position) 
	      (line-beginning-position 2)))))

(defadvice occur (around occur-region)
  (save-restriction
    (if (and mark-active transient-mark-mode)
        (narrow-to-region (region-beginning) (region-end)))
    ad-do-it))
(ad-activate 'occur)


(defun dwim-paren-region (arg leftp rightp)
  (if mark-active
    (save-excursion
      (let ((beg (min (point) (mark)))
            (end (max (point) (mark))))
        (goto-char beg)
	(insert leftp)
        (goto-char (1+ end))
	(insert rightp))
    )
    (insert arg)))
(global-set-key "(" (lambda()(interactive)(dwim-paren-region "(" "(" ")"))) 
(global-set-key ")" (lambda()(interactive)(dwim-paren-region ")" "(" ")")))
(global-set-key "{" (lambda()(interactive)(dwim-paren-region "{" "{" "}")))
(global-set-key "}" (lambda()(interactive)(dwim-paren-region "}" "{" "}")))
(global-set-key "[" (lambda()(interactive)(dwim-paren-region "[" "[" "]")))
(global-set-key "]" (lambda()(interactive)(dwim-paren-region "]" "[" "]")))



;; (require 'cua)
;;(my-cua-bindings)


(defun backward-kill-space()
  (interactive)
  (set-mark-command nil)
  (skip-chars-backward " \t")
  (kill-region (point) (mark))
  )
  
(defun backward-kill-word-dwim()
  (interactive)
  (cond ((looking-back "\t") (backward-kill-space))
	((looking-back " ") (progn (backward-delete-char 1)
				   (cond ((looking-back "[_a-zA-Z0-9]") (backward-kill-word 1))
					 (t (backward-kill-space))
					 )
				   ))
	((looking-back "[_a-zA-Z0-9]") (backward-kill-word 1))
	(t (progn (backward-delete-char 1)
		  (cond ((looking-back "[_a-zA-Z0-9]") (backward-kill-word 1))
		   )
		  ))
	)
)

;; (pc-selection-mode t)				;; put it before windmove bindings
(global-set-key "\M-\d" 'backward-kill-word-dwim)	;; bind it back
;; (global-set-key "\M-w" 'kill-ring-save)		;; bind it back
;; (global-set-key "\C-x\C-x" 'exchange-point-and-mark-dwim)

;;  (windmove-default-keybindings)		;; default S-left bindings conflict with xterm
  (global-set-key [(meta left)]  'windmove-left)
  (global-set-key [(meta up)]    'windmove-up)
  (global-set-key [(meta right)] 'windmove-right)
  (global-set-key [(meta down)]  'windmove-down)

