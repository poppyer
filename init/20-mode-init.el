(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))

(defun common-local-setting()
  (setq truncate-lines t)

  (unless (get major-mode 'local-done)		;; once re-binding once for each major mode
;;   (lexical-let ...)

;;  (setq indent-tabs-mode t)			; work in text but not in c mode
  (local-set-key (kbd "DEL") 'backward-delete-char) ; otherwise untabify
					; still not working in lisp

(require 'cl)
;; TO DO TAB + M-TAB
  (lexical-let ((tab (key-binding (kbd "TAB")))
		(mtab (key-binding (kbd "M-TAB"))))
    (local-set-key (kbd "TAB") (lambda()
				 (interactive)
				 (if (looking-at "\\>")
				     (command-execute mtab)
				   (command-execute tab)
				   )
				 )
		   )
    )	

   (put major-mode 'local-done t))
 )

(add-hook 'text-mode-hook 'common-local-setting)
(add-hook 'c-mode-common-hook 'common-local-setting)
(add-hook 'emacs-lisp-mode-hook 'common-local-setting)
(add-hook 'lisp-interaction-mode-hook 'common-local-setting)


(add-hook 'c-mode-common-hook (lambda()
				(make-local-variable 'skeleton-pair-alist)
				(setq skeleton-pair-alist  '(
							     (?` ?` _ "''")
							     (?\( ?  _ " )")
							     (?\[ ?  _ " ]")
							     (?{ \n > _ \n ?} >)))
				(setq skeleton-pair t)
				(local-set-key (kbd "{") 'skeleton-pair-insert-maybe)
				(c-set-style "java") ;; use TAB to indent
				;; linux TAB = 8
				;; java TAB = 4
				(modify-syntax-entry ?_ "w") ;; make the _ part of a word.
				))   


;; not good for Alt-Backspace
;; (add-hook 'emacs-lisp-mode-hook (lambda()
;; 				  (modify-syntax-entry ?- "w")
;; 				  ))


;; For Dired Mode
(setq dired-listing-switches "-l")	;; hide .hiding file


;; For tex mode
(setq TeX-command-force "") ;; save a RETURN key
;; ln -s xdvi dvipdfm 
;; (setq LaTeX-command "latex --output-format=pdf")
(setq TeX-output-view-style
'(("^dvi$"
  ("^landscape$" "^pstricks$\\|^pst-\\|^psfrag$")
  "%(o?)dvips -t landscape %d -o && gv %f")
 ("^dvi$" "^pstricks$\\|^pst-\\|^psfrag$" "%(o?)dvips %d -o && gv %f")
 ("^dvi$"
  ("^a4\\(?:dutch\\|paper\\|wide\\)\\|sem-a4$" "^landscape$")
  "%(o?)xdvi %dS -paper a4r -s 0 %d")
 ("^dvi$" "^a4\\(?:dutch\\|paper\\|wide\\)\\|sem-a4$" "%(o?)xdvi %dS -paper a4 %d")
 ("^dvi$"
  ("^a5\\(?:comb\\|paper\\)$" "^landscape$")
  "%(o?)xdvi %dS -paper a5r -s 0 %d")
 ("^dvi$" "^a5\\(?:comb\\|paper\\)$" "%(o?)xdvi %dS -paper a5 %d")
 ("^dvi$" "^b5paper$" "%(o?)xdvi %dS -paper b5 %d")
 ("^dvi$" "^letterpaper$" "%(o?)xdvi %dS -paper us %d")
 ("^dvi$" "^legalpaper$" "%(o?)xdvi %dS -paper legal %d")
 ("^dvi$" "^executivepaper$" "%(o?)xdvi %dS -paper 7.25x10.5in %d")
 ("^dvi$" "." "%(o?) xdvi %dS %d")         ;; by poppyer xdvi => dvipdfm, but slow?
 ("^pdf$" "." "xpdf -remote %s -raise %o %(outpage)")
 ("^html?$" "." "netscape %o")))



(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'longlines-mode)
;;(add-hook 'LaTeX-mode-hook 'visible-mode)
(add-hook 'LaTeX-mode-hook 'outline-minor-mode)
(setq LaTeX-indent-level 4)

;; flyspell/longlines-mode is slow when for large file
;; (add-hook 'text-mode-hook '(lambda()
;; ;;                         (flyspell-mode t)
;;                               (setq indent-tabs-mode t)
;; ;;			      (set-fill-column 78)
;; ;;			      (longlines-mode t)
;;                               )
;;                               )
