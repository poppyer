;; eim wb input method
(add-to-list 'load-path "~/.emacs.d/eim")
(autoload 'eim-use-package "eim" "Another emacs input method")
(setq eim-use-tooltip nil)
;;(define-key eim-mode-map "\C-g" 'eim-quit-clear)

(register-input-method
 "eim-wb" "euc-cn" 'eim-use-package
 "五笔" "汉字五笔输入法" "wb.txt")

(setq-default eim-wb-history-file nil)

(defun eim-clear()
  (interactive)
(progn
  (setq eim-package-list nil
        eim-current-package nil
        eim-wb-package nil
        eim-wb-initialized nil)
  (mapc
   (lambda (buf)
     (if (string-match (regexp-quote " *eim-wb*") (buffer-name))
         (kill-buffer buf)))
   (buffer-list)))
)

;; (let ((eim-current-package eim-wb-package))
;;   (eim-set-option 'record-position nil))

(setq default-input-method "eim-wb")


;; 用 ; 暂时输入英文
;;(require 'eim-extra)
(autoload 'eim-insert-ascii "eim-extra" "Use ; to insert ascii")
(global-set-key ";" 'eim-insert-ascii)


(defvar eim-punc-translate-p t
  "*Non-nil means will translate punctuation.")
(defun eim-punc-translate (punc-list char)
  (if eim-punc-translate-p
      (cond ((< char ? ) "")
            ((and eim-insert-ascii-char
                  (= char (car eim-insert-ascii-char)))
             (char-to-string char))
            (t (let ((str (char-to-string char))
                     punc)
                 (if (and (not (member (char-before) eim-punc-escape-list))
                          (setq punc (cdr (assoc str punc-list))))
                     (progn
                       (if (= char (char-before))
                           (delete-char -1))
                       (if (= (safe-length punc) 1)
                           (car punc)
                         (setcdr (cdr punc) (not (cddr punc)))
                         (if (cddr punc)
                             (car punc)
                           (nth 1 punc))))
                   str))))
    (char-to-string char)))
(defun eim-punc-translate-toggle (arg)
  (interactive "P")
  (setq eim-punc-translate-p
        (if (null arg)
            (not eim-punc-translate-p)
          (> (prefix-numeric-value arg) 0))))

;; end of eim

