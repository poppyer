
(global-set-key "\C-x\C-r" 'revert-buffer)
;;(global-auto-revert-mode t)		


(global-set-key (kbd "C-x SPC") (lambda()		;; toggle-mark
				  (interactive)
				  (if mark-active
				      (deactivate-mark)
				    (cons
				     (exchange-point-and-mark)
				     (exchange-point-and-mark)
				     )
				    )
				  )
		)



(global-set-key "\C-x\C-j" 'delete-indentation)

;;CRM bufer list;; ibuffer is another choice
(if (string-match "XEmacs" (emacs-version))
    (global-set-key "\C-x\C-b" 'electric-buffer-list)
  (global-set-key "\C-x\C-b" 'bs-show)
)

(defun kill-other-buffer ()
  (interactive)
  (if (one-window-p t)
      (message "No other window")
    (other-window 1)
    (kill-this-buffer)
    (other-window 1)
    )
  )
(global-set-key "\C-x\M-k" 'kill-other-buffer)


(defun switch-to-previous-buffer ()
  (interactive)
  (switch-to-buffer (other-buffer)))
(global-set-key "\M-o" 'switch-to-previous-buffer)



;;(global-set-key "\M-g" 'goto-line)		;; use \M-g \M-g by default
(global-set-key "\M-gf" 'facemenu-set-foreground)
(global-set-key "\M-gB" 'facemenu-set-background)
(global-set-key "\M-gb" 'facemenu-set-bold)
(global-set-key "\M-g\M-c" 'calculator)
(global-set-key "\M-#" 'calc-dispatch)




(defun wy-go-to-char (n char)
  "Move forward to Nth occurence of CHAR.
Typing `wy-go-to-char-key' again will move forward to the next Nth occurence of CHAR."
  (interactive "p\ncGo to char: ")
  (search-forward (string char) nil nil n)
  (while (char-equal (read-char)
                     char)
    (search-forward (string char) nil nil n))
  (setq unread-command-events (list last-input-event)))

(defun wy-backward-to-char (n char)
  "Move backward to Nth occurence of CHAR.
Typing `wy-backward-to-char-key' again will move backward to the next Nth occurence of CHAR."
  (interactive "p\ncBackward to char: ")
  (backward-char)
  (search-backward (string char) nil nil n)
  (forward-char)
  (while (char-equal (read-char)
                     char)
    (search-backward (string char) nil nil (+ n 1))
    (forward-char))
  (setq unread-command-events (list last-input-event)))

(define-key global-map (kbd "C-c f") 'wy-go-to-char)
(define-key global-map (kbd "C-c b") 'wy-backward-to-char)



;; ;;;;;;;;;;;;;;;;;;;;;; simple Bookmarking - history stack ;;;;;;;;;;;;;;;
;; (global-set-key [f11] 'point-stack-push)
;; (global-set-key [f10] 'point-stack-pop)
;;(defvar point-stack nil)
;; (defun point-stack-push ()
;;   "Push current location and buffer info onto stack."
;;   (interactive)
;;   (message "Location marked.")
;;   (setq point-stack (cons (list (current-buffer) (point)) point-stack)))
;; (defun point-stack-pop ()
;;   "Pop a location off the stack and move to buffer"
;;   (interactive)
;;   (if (null point-stack)
;;       (message "Stack is empty.")
;;     (switch-to-buffer (caar point-stack))
;;     (goto-char (cadar point-stack))
;;     (setq point-stack (cdr point-stack))))
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun mark-and-jump (&optional arg)
  "remember current point and jump to previous"
  (interactive "P")
  (let ((tmp (point-marker)))
    (when (null arg)
      (jump-to-register ?j))
    (set-register ?j tmp))
  )
(global-set-key [f11] 'mark-and-jump)
(global-set-key [M-f11] '(lambda()
			   (interactive)
			   (mark-and-jump -1)
			   )
		)



;;;;;;;;;;;;;;;;;;;;;;;;;;history;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (defun previous-history-matching-element (n)
    (interactive "p")
    (unless (memq last-command '(previous-history-matching-element
				 next-history-matching-element))
      (setq minibuffer-text-before-history (minibuffer-contents)))
    (previous-matching-history-element
     (concat "^" (regexp-quote minibuffer-text-before-history)) n))
  (defun next-history-matching-element (n)
    (interactive "p")
    (previous-history-matching-element (- n)))
  (mapc (lambda (map)
	  (define-key map (kbd "C-p") 'previous-history-matching-element)
	  (define-key map (kbd "C-n") 'next-history-matching-element))
	(list minibuffer-local-completion-map
	      minibuffer-local-map
	      minibuffer-local-must-match-map
	      minibuffer-local-ns-map))




;;(set-default-font "-misc-fixed-medium-*-normal--15-*")
;;(add-to-list 'default-frame-alist '(font . "9x15"))
(global-set-key (kbd "s-f") 'new-frame)
