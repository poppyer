;;jdee
(add-to-list 'load-path (expand-file-name "~/.emacs.d/cedet/common"))
(load-library "cedet.elc")
(add-to-list 'load-path (expand-file-name "~/.emacs.d/jde/lisp"))
(add-to-list 'load-path (expand-file-name "~/.emacs.d/elib"))

(setq defer-loading-jde t)
(if defer-loading-jde
    (progn
      (autoload 'jde-mode "jde" "JDE mode." t)
      (setq auto-mode-alist
	        (append
		      '(("\\.java\\'" . jde-mode))
		           auto-mode-alist)))
  (require 'jde))

(defun jde-init ()
  (interactive)

  (jde-gen-define-abbrev-template "sout" '("System.out.println(" 'r ");"))
  (setq jde-complete-insert-method-signature nil)
  (setq jde-debugger '("JDEbug"));

(custom-set-variables
 '(jde-project-context-switching-enabled-p nil)
 '(jde-key-bindings 
  (list 
   (cons "[?\C-c ?\C-v ?\C-a]" 'jde-run-menu-run-applet)
   (cons "[?\C-c ?\C-v ?\C-b]" 'jde-build)
   (cons "[?\C-c ?\C-v ?\C-c]" 'jde-compile)
   (cons "[?\C-c ?\C-v ?\C-d]" 'jde-debug)
   (cons "[?\C-c ?\C-v ?\C-f]" 'jde-find)
   (cons "[?\C-c ?\C-v ?\C-g]" 'jde-open-class-at-point)
   (cons "[?\C-c ?\C-v ?\C-k]" 'jde-bsh-run)
;   (cons "[?\C-c ?\C-v ?\C-l]" 'jde-gen-println)
   (cons "[?\C-c ?\C-v ?\C-l]" 'jde-load-project-file)
   (cons "[?\C-c ?\C-v ?\C-n]" 'jde-help-browse-jdk-doc)
   (cons "[?\C-c ?\C-v ?\C-p]" 'jde-save-project)
   (cons "[?\C-c ?\C-v ?\C-q]" 'jde-wiz-update-class-list)
   (cons "[?\C-c ?\C-v ?\C-r]" 'jde-run)
   (cons "[?\C-c ?\C-v ?\C-s]" 'speedbar-frame-mode)
   (cons "[?\C-c ?\C-v ?\C-t]" 'jde-jdb-menu-debug-applet)
   (cons "[?\C-c ?\C-v ?\C-w]" 'jde-help-symbol)
   (cons "[?\C-c ?\C-v ?\C-x]" 'jde-show-superclass-source)
   (cons "[?\C-c ?\C-v ?\C-y]" 'jde-open-class-at-point)
   (cons "[?\C-c ?\C-v ?\C-z]" 'jde-import-find-and-import)
   (cons "[?\C-c ?\C-v ?e]"    'jde-wiz-extend-abstract-class)
   (cons "[?\C-c ?\C-v ?f]"    'jde-gen-try-finally-wrapper)
   (cons "[?\C-c ?\C-v ?i]"    'jde-wiz-implement-interface)
   (cons "[?\C-c ?\C-v ?j]"    'jde-javadoc-autodoc-at-line)
   (cons "[?\C-c ?\C-v ?o]"    'jde-wiz-override-method)
   (cons "[?\C-c ?\C-v ?t]"    'jde-gen-try-catch-wrapper)
   (cons "[?\C-c ?\C-v ?z]"    'jde-import-all)
   (cons "[?\C-c ?\C-v ?\C-[]" 'jde-run-etrace-prev)
   (cons "[?\C-c ?\C-v ?\C-]]" 'jde-run-etrace-next)
   (cons "[(control c) (control v) (control ?.)]" 'jde-complete)
   (cons "[(control c) (control v) ?.]" 'jde-complete-in-line)
 
   (cons "\M-." 'jde-complete-in-line)
   (cons "[f9]" 'jde-compile)
   (cons "[f6]" 'jde-run)
   )
))
)