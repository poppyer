;; Make shifted direction keys work on the GNU/Linux console or in an xterm
;;(when (or (string= (getenv "TERM") "linux")
;;	  (string-match "^xterm" (getenv "TERM"))
;;	  (string= (getenv "TERM") "screen"))
;;    (define-key function-key-map "\eO2A" [S-up])
;;    (define-key function-key-map "\eO2B" [S-down])
;;    (define-key function-key-map "\eO2D" [S-left])
;;    (define-key function-key-map "\eO2C" [S-right])
;;    (define-key function-key-map "\e[1;2F" [S-end])
;;    (define-key function-key-map "\e[1;2H" [S-home])
;;    (define-key function-key-map "\e[5;3~" [M-prior])
;;    (define-key function-key-map "\e[6;3~" [M-next])
  (require 'xterm-extras)
  (xterm-extra-keys)
;;)

(define-key function-key-map "\eOH" [home])
(define-key function-key-map "\eOF" [end])
(define-key function-key-map "\e[7~" [home])
(define-key function-key-map "\e[8~" [end])


;; for MAC
(define-key function-key-map "\eO1;3A" [M-up])
(define-key function-key-map "\eO1;3B" [M-down])
(define-key function-key-map "\eO1;3D" [M-left])
(define-key function-key-map "\eO1;3C" [M-right])


;(global-set-key [home] 'beginning-of-line)
;(global-set-key [end] 'end-of-line)
