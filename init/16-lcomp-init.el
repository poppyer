
(require 'lcomp)	;; auto close completion buffer
(lcomp-install)

(add-hook 'compilation-finish-functions	; auto close compilation
      (lambda (buf str)
        (if (string-match "exited abnormally" str) 
	    (next-error)
          ;;no errors, make the compilation window go away in a few seconds
          (run-at-time "2 sec" nil 'delete-windows-on (get-buffer-create "*compilation*"))
          (message "No Compilation Errors!")
	  )
	))
