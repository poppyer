

;;; Emacs Load Path
(setq load-path (cons "~/.emacs.d/lisp" load-path))

;;; load init files
(let ((default-directory "~/.emacs.d/init"))
  (dolist (file (mapcar 'file-name-sans-extension
			(file-expand-wildcards "*-init.el")))
    (eval-after-load (intern (progn (string-match "\\(.*\\)-init" file)
				    (match-string 1 file)))
      (load (expand-file-name file))))
)

;; set backup dir
(setq backup-directory-alist '(("." . "~/.emacs.d/backup")))
(setq revert-without-query '(".*\\.tmp" ".*\\.log"))

;; font setting
(defun my-default-font()
  (interactive)
  (set-default-font "-apple-monaco-medium-r-normal--13-0-72-72-m-0-iso10646-1")
;;  (set-fontset-font "fontset-default"	
;;		    'han '("Firefly New Sung" . "unicode-bmp"))
  )
(my-default-font)
(add-to-list 'after-make-frame-functions
	     (lambda (new-frame) 
	       (select-frame new-frame) 
	       (my-default-font)
	       ))

;;(require 'erc)		;; will read .ercrc.el
(autoload 'erc "erc" "erc for IRC" t)
(autoload 'hanconvert-region "hanconvert"
   "Convert a region from simple chinese to trandition chinese or from trandition chinese to simple chinese" t)

;; (autoload 'longlines-mode
;;     "longlines.el"
;;     "Minor mode for automatically wrapping long lines." t)
(setq longlines-wrap-follows-window-size t)
(setq window-min-width 1)
(setq window-min-height 1)
(setq compilation-window-height 10)

;; (require 'gnuserv-compat)
;; (require 'gnuserv)
;; (gnuserv-start)
;; (when (or (not (boundp 'server-process))
;; 	  (not (eq (process-status server-process) 'listen)))

;; (setq server-use-tcp t)
;; (setq server-host
;;       (if (executable-find "/sbin/ip")
;;           (let ((i (shell-command-to-string "/sbin/ip route get 1")))
;;             (string-match " src \\([0-9.]+\\) " i)
;;             (match-string 1 i))
;;         nil))
(if (string-match "XEmacs" (emacs-version))
    (gnuserv-start)
  (server-start)
  )
;;)

(desktop-load-default)					
;;(desktop-read)


(unless (string-match "XEmacs" (emacs-version))  
  ;;(require 'tabbar)
;; ;  (require 'pinbar)
;;   (global-set-key "\M-0" 'pinbar-add)
;;   (pinbar-mode t)
;;   ;;(global-set-key "\M-9" 'pinbar-del)
;;   (global-set-key "\M-8" 'bs-cycle-previous)
;;   (global-set-key "\M-9" 'bs-cycle-next)


  ;; In every buffer, the line which contains the cursor will be fully highlighted,
  ;; UGLY-GREEN
  (global-hl-line-mode 1)		     ;; conflict with latex color
  ;; ALL THE COLOR_SETTINGS goto custermize at the end
  ;; To customize the background color


  ;; ========== Support Wheel Mouse Scrolling ==========
  ;; use "emacs -nw" rather than "emacs-nox" otherwise these not working
;;  (xterm-mouse-mode t) ;; it breaks middle-key paste in Mac OSX
  (mouse-wheel-mode t)
  (mouse-sel-mode t)
  )

(setq scroll-preserve-screen-position t)

;;(global-set-key (kbd "TAB") 'tab-to-tab-stop)  ; default is M-i

(setq default-tab-width 4)
(setq tab-stop-list (let ((stops (list default-tab-width)))
					  (while (< (car stops) 120)
						(setq stops (cons (+ default-tab-width (car stops)) stops)))
					  (nreverse stops)))

;;(global-set-key (kbd "DEL") 'backward-delete-char) ; not working?
;; a work-around for above?	reset works although it has been set before

(global-set-key (kbd "<delete>") 'delete-char)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

(setq-default transient-mark-mode t) 
(setq-default truncate-lines nil)    ;; display long lines in read-only buffer
(setq truncate-partial-width-windows nil) 
(setq-default case-fold-search t)
;;(default-input-method "chinese-py-punct")
(global-font-lock-mode t)
(show-paren-mode t)
(which-function-mode t)

;;(setq visible-bell t)
(fset 'yes-or-no-p 'y-or-n-p)
(setq require-final-newline nil)
;;(setq inhibit-default-init t)		;; no /usr/share/emacs/site-list/default.el, not working

;;(iswitchb-default-keybindings)
(iswitchb-mode t)
(add-to-list 'iswitchb-buffer-ignore "*Help*")
(add-to-list 'iswitchb-buffer-ignore "*TeX Help*")
(add-to-list 'iswitchb-buffer-ignore "*Completions*")
(add-to-list 'iswitchb-buffer-ignore ".newsticker-cache")
(add-to-list 'iswitchb-buffer-ignore ".newsrc-dribble")
(add-to-list 'iswitchb-buffer-ignore "*buffer-selection*")
(add-to-list 'iswitchb-buffer-ignore "*Apropos*")
(add-to-list 'iswitchb-buffer-ignore "*Server*")
(add-to-list 'iswitchb-buffer-ignore "localhost:6667")
(add-to-list 'iswitchb-buffer-ignore "*Summary.*")


;;(ido-mode t)			;; can not use ctl-p in open new file

;; Supress the GNU startup message
(setq inhibit-startup-message t)

;; Display time
;; (setq display-time-day-and-date t
;;    display-time-24hr-format t)
;; (display-time)

;; ===== Set standard indent to 2 rather that 4 ====
(setq standard-indent 8)

;; ========== Line by line scrolling ========== , sometimes 1 is problematic
(setq scroll-step 2)
(setq hscroll-step 1)



(setq-default cursor-type 'bar)

;; Show line-number in the mode line
;;(line-number-mode 1)
;; Show column-number in the mode line
(column-number-mode 1)

;; ===== Make Text mode the default mode for new buffers =====, problemic with ll
;;(setq default-major-mode 'text-mode)

;; (setq load-path (cons "/share/predictive" load-path))
;; (autoload 'predictive-mode "predictive" "predictive" t)

;; ;; can not use in transient-mark-mode of emacs23
;; (defadvice message (before when-was-that activate)
;;     "Add timestamps to `message' output."
;;     (ad-set-arg 0 (concat (format-time-string "[%T] ") 
;;                           (ad-get-arg 0)) ))

(setq minibuffer-message-timeout 0.1)

(setq warning-suppress-types '((undo discard-info)))

(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(browse-url-browser-function (quote browse-url-firefox))
 '(canlock-password "448c4792971128243b8d7f3d509d2452fc2c38a1")
 '(eshell-output-filter-functions (quote (eshell-handle-control-codes eshell-watch-for-password-prompt eshell-postoutput-scroll-to-bottom)))
 '(eshell-scroll-show-maximum-output t)
 '(eshell-scroll-to-bottom-on-output nil)
 '(newsticker-url-list (quote (("LinuxTOY" "http://feeds.feedburner.com/linuxtoy" nil nil nil) ("LinuxByExamples" "http://feeds.feedburner.com/LinuxByExamples" nil nil nil) ("Distrowatch" "http://distrowatch.com/news/dw.xml" nil nil nil) ("New Packages" "http://distrowatch.com/news/dwp.xml" nil nil nil) ("kissingbaby" "http://www.newsmth.net/pc/rss.php?userid=kissingbaby" nil nil nil) ("all-things-emacs" "http://feeds.feedburner.com/emacsblog" nil nil nil))))
 '(scroll-bar-mode (quote left))
;;  '(tool-bar-mode nil)
 '(weblogger-config-alist (quote (("freeweb7" ("user" . "poppyer") ("server-url" . "http://poppyer.freeweb7.com/wp/xmlrpc.php") ("weblog" . "1"))))))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(diary ((((class color) (background dark)) (:background "cyan"))))
 '(font-lock-comment-face ((((class color)) (:foreground "red"))))
 '(variable-pitch ((t (:family "DejaVu Serif")))))

(put 'scroll-left 'disabled nil)

;; eshell to scroll


;;Status bar color
(unless (or (string-match "XEmacs" (emacs-version))
	     window-system		;; xwindow test
	     )
  
;;   (set-face-foreground 'modeline "white")
;;   (set-face-background 'modeline "black")

;;   (custom-set-faces
;;    '(highlight ((((background dark)) (:background "white"))))
;;    '(minibuffer-prompt ((((background dark)) (:weight bold))))
;;    '(which-func ((((background dark)) (:weight bold))))
;;    )
  (menu-bar-mode nil)
;;   (set-face-background 'hl-line "white")  ;; Emacs 22 Only, destory foreground
  ;;  (set-face-background 'highlight "white")  ;; Emacs 21 Only
  )
