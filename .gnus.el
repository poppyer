(setq gnus-use-dribble-file nil)
;; (setq gnus-invalid-group-regexp "[:`'\"]\\|^$")

(setq gnus-select-method

;;  ; a ssl server on a non-standard port:
;;  '(nnimap "herald"
;; 		(nnimap-authenticator login)
;;      (nnimap-address "ball2148.herald.ox.ac.uk")
;;      (nnimap-server-port 993)
;; ;		(nnimap-list-pattern ("INBOX" "mail/*" "Mail/*" "INBOX.*"))
;; 		(nnimap-list-pattern ("*"))
;; 		(nnimap-expunge-on-close ask)
;;      (nnimap-stream ssl)
;; 	)

	'(nnimap "gmail"
;			 (nnimap-authenticator login)
			 (nnimap-address "imap.gmail.com")
			 (nnimap-server-port 993)
			 (nnimap-list-pattern ("*"))
			 (nnimap-expunge-on-close ask)
			 (nnimap-stream ssl))
)

;; Filtering
(setq nnimap-split-inbox '("INBOX"))
;this is slow, coz it need to get all active files
(setq nnimap-split-predicate "UNDELETED")
(setq nnimap-split-predicate "UNSEEN UNDELETED")
(setq nnimap-split-crosspost nil)
(setq nnimap-split-rule
      '(
        ("Junk"		"^To:.*OxfordFreecycle@yahoogroups.com")
	("Junk"		"^To:.*balliolmcr@maillist.ox.ac.uk")
	("Junk"		"^CC:.*balliolmcr@maillist.ox.ac.uk")
	("Junk"		"^To:.*ambassadors@Sun.COM")
	("Junk"		"^CC:.*ambassadors@Sun.COM")
	("Junk"		"^To:.*kwiat@well.ox.ac.uk")
	("Junk"		"^X-Oxmail-Spam-Level: \\*\\*\\*\\*")
	)
)


(setq message-cite-prefix-regexp
      "\\([ \t]*[-_.[:word:]]+>+\\|[ \t]*[]>|}+:]\\)+")


	
;; layout
(gnus-add-configuration
 '(article (vertical 1.0
		     ;;               (group 4)
		     (summary .4 point)
		     (article 1.0)
	       )
	   ))


;; show only text from html?
(eval-after-load "mm-decode"
  '(progn
     (add-to-list 'mm-discouraged-alternatives "text/html")
     (add-to-list 'mm-discouraged-alternatives "text/richtext")))
(setq gnus-mime-display-multipart-as-mixed t)
(setq mm-inline-override-types '("text/html"))

(add-hook 'gnus-article-mode-hook '(lambda ()
				     (setq truncate-lines nil)
				     ));;also can use W w each time

(defun jump-skip-citation ()
  (interactive)
				  (gnus-summary-select-article-buffer)
  (search-forward-regexp "^[^>\:][^\n:@]+$")
  (beginning-of-line)
  (recenter)
				  (gnus-article-show-summary)
  )

;; (add-hook 'gnus-select-article-hook '(lambda()			;; seems nor working!!
;; 				  (jump-skip-citation)
;; 				  ));


(require 'ansi-color)
(defun article-treat-ansi-sequences ()
  "Translate ANSI SGR control sequences into overlays or extents."
  (interactive)
  (save-excursion
    (set-buffer gnus-article-buffer)
    (when (article-goto-body)
      (let ((buffer-read-only nil))
        (ansi-color-apply-on-region (point) (point-max))
	(fix-screen-encoding-bug)
	))))

(defun fix-screen-encoding-bug()
  (interactive)
	(replace-string "��" 
			" \"" nil (point-min) (point-max))
	(replace-string "��"
			"\" " nil (point-min) (point-max))
	(replace-string "��"
			"��" nil (point-min) (point-max))
	(replace-string "��"
			"<<" nil (point-min) (point-max))
	(replace-string "��"
			">>" nil (point-min) (point-max))
	(replace-string "��"
			": " nil (point-min) (point-max))
	(replace-string "��"
			"..." nil (point-min) (point-max))
  )

(add-hook 'gnus-article-prepare-hook 'article-treat-ansi-sequences)
;;(add-hook 'gnus-summary-prepare-hook 'fix-screen-encoding-bug)

;; using decode-hook but no color, using display-hook is good
;;(set 'gnus-article-decode-hook (cdr gnus-article-decode-hook))

(add-hook 'gnus-summary-mode-hook
          '(lambda ()
            (local-set-key "\M-j" 'jump-skip-citation)
	    (local-set-key "\M-c" 'article-treat-ansi-sequences)
            (local-set-key [up] '(lambda ()
                                  (interactive)
                                  (when (> (count-windows) 1)
                                    (delete-other-windows))
                                  (previous-line 1)))
            (local-set-key [down] '(lambda ()
                                    (interactive)
                                    (when (> (count-windows) 1)
                                      (delete-other-windows))
                                    (next-line 1)))
            (local-set-key [\C-return] '(lambda ()
                                         (interactive)
                                         (gnus-summary-next-page)))))



(defun bounce-server ()
  (interactive)
  (let ((server (gnus-group-server (gnus-group-group-name))))
    (progn
     (gnus-server-close-server server)
     (message "server closed, now reopen it...")
     (gnus-server-open-server server)
     (message "server reopened"))))

(add-hook 'gnus-group-mode-hook
	  '(lambda()
	     (local-set-key "O" 'bounce-server)
	     (local-set-key "\M-c" 'calculator)
	     )
	  )

;; Hook to turn on autofill
(defun my-message-mode-setup ()
  (setq fill-column 72)
  (turn-on-auto-fill)
  (flyspell-mode t))
(add-hook 'message-mode-hook 'my-message-mode-setup)

;; Expire days
(setq nnmail-expiry-wait 2)

(load-library "gnus-group") ;; Needed for gnus-group-list-all-groups :P
(gnus-group-list-all-groups) ;; Show all the groups, even empty ones


(setq gnus-read-active-file 'some)
;;(add-hook 'gnus-group-mode-hook 'gnus-topic-mode)
;; fetch old headers when a new follow-up arrives		;; but t is slow, poppyer say
(setq gnus-fetch-old-headers nil)
(setq gnus-asynchronous t)
(setq gnus-use-article-prefetch nil)
(setq gnus-save-newsrc-file nil)

(setq gnus-extract-address-components
      'mail-extract-address-components)

(defsubst gnus-user-format-function-S (header)
  "Returns message size in kB for `gnus-summary-line-format', if greater than
`gnus-summary-line-show-size-threshold'."
  (let* ((c (or (mail-header-chars header) 0)))
    (if (> c gnus-summary-line-show-size-threshold)
	(format "%3dk " (/ c 1024.0))
      "")))
(defvar gnus-summary-line-show-size-threshold 10000
  "(juhp) Value above which message size is shown in my summary line.")
(setq gnus-summary-line-format "%U%R%z%6uS %I%d %(%-20,20f:%) %s\n")
(setq gnus-group-line-format "%M%S%P%3L%3y: %(%-40g%)%O %2{%d%} %l \n")
(setq gnus-article-mode-line-format "Gnus: %g [%w] %m")		;; some subjects are too long





(require 'gnus-demon)

(setq level 2)

(defun sk-demon-notify-message ()
  (if (> number-of-unread last-time-number-of-unread)
      (progn
        (if (> number-of-unread 0)
            (beep))
        (message (let ((number-of-new (- number-of-unread last-time-number-of-unread)))
                   (concat
                    "***** "
                    (int-to-string number-of-new)
                    " new mail"
                    (if (> number-of-new 1)
                        "s" "")
                    " received *****"
                    (if (> number-of-unread last-time-number-of-unread)
                        (concat " ("
                                (int-to-string (- number-of-unread number-of-new))
                                " old)"))
                    )
                   )
                 )
        )
    (if (= number-of-unread 0)
        (message "No mail")
      (message
       (concat
        (int-to-string number-of-unread)
        " old unread mail"
        (if (> number-of-unread 1)
            "s" "")
        ))
      )
    )
  )

(defun sk-gnus-demon-handler ()
  (message "Checking new mail ...")
  (save-window-excursion
    (save-excursion
      (when (gnus-alive-p)
        (set-buffer gnus-group-buffer)
        (gnus-group-get-new-news level))))
  (setq last-time-number-of-unread number-of-unread)
  (setq number-of-unread (gnus-group-number-of-unread-mail level))
  (sk-demon-notify-message)
  )

;(setq gnus-demon-timestep 10)

;(gnus-demon-add-handler 'sk-gnus-demon-handler 5 nil)
(gnus-demon-add-handler 'gnus-group-get-new-news 15 nil)
;(gnus-demon-add-handler 'gnus-demon-scan-news 5 10)
;(gnus-demon-init)


(require 'gnus-notify)

     (setq gnus-thread-sort-functions
           '((lambda (t1 t2)
               (not (gnus-thread-sort-by-date t1 t2)))
             gnus-thread-sort-by-score))




(require 'gnushush)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; SMTP
(setq message-send-mail-function 'smtpmail-send-it) ; for message/Gnus
(setq send-mail-function 'smtpmail-send-it) ; for C-x m, etc.
(setq smtpmail-debug-info t)


(setq message-cite-function
      'message-cite-original-without-signature)
(setq message-citation-line-function '(lambda()
					(newline-and-indent)
					(message-insert-citation-line)
					(message-goto-body)
					)
      )

;;The message buffer will be killed after sending a message.
(setq message-kill-buffer-on-exit t)

(setq gnus-gcc-mark-as-read t)  ;;automatically mark sent as read        


;;(setq gnus-message-archive-group "Sent") ;; Save sent mail, but for nnfolder
(setq gnus-posting-styles
      '((".*"
	 ;; seems smtp internal "return-to/evelop-from" address
	 (name (setq user-full-name "Gaofeng Huang"))
	 (address (setq user-mail-address "Gaofeng.Huang@balliol.ox.ac.uk"))
	 (gcc "Sent")		;; local copy, good for later forward
	 (bcc "")
;	 (bcc "gfhuang@gmail.com")
	 )
        ("^nntp.*"
	 ;; seems smtp internal "return-to/evelop-from" address
	 (name (setq user-full-name "poppyer"))
	 (address (setq user-mail-address "poppyer@gmail.com"))
	 (gcc "")
	 (bcc "")
	 )
	((header "Delivered-To" "poppyer@gmail.com")
	 (name (setq user-full-name "poppyer"))
	 (address (setq user-mail-address "poppyer@gmail.com"))
	 (gcc "")
	 (bcc "")
	 )
	))



;; CRITICAL for using starttls
;; SMTP is not related to NNTP (newsgroups posting)
(setq starttls-use-gnutls nil)


;; (setq smtpmail-default-smtp-server "smtp.ox.ac.uk"
;;       smtpmail-smtp-server "smtp.ox.ac.uk"
;;       smtpmail-smtp-service 25
;; )

(setq smtpmail-starttls-credentials '(("smtp.gmail.com" 25 nil nil))
      smtpmail-auth-credentials '(("smtp.gmail.com" 25 "poppyer@gmail.com" nil))
      smtpmail-default-smtp-server "smtp.gmail.com"
      smtpmail-smtp-server "smtp.gmail.com"
      smtpmail-smtp-service 25
      smtpmail-local-domain "yourcompany.com")

;; FOR GMAIL SMTP

;; (setq smtpmail-smtp-server "smtp.gmail.com")
;; (setq smtpmail-smtp-service 587)
;; (setq smtpmail-auth-credentials '(("smtp.gmail.com" 587 "poppyerfs@gmail.com" nil)))
;; (setq smtpmail-starttls-credentials '(("smtp.gmail.com" 587 nil nil)))


;; (defun fs-change-smtp ()
;;   "Change the SMTP server according to the current from line."
;;   (save-excursion
;;     (let ((from
;;            (save-restriction
;;              (message-narrow-to-headers)
;;              (message-fetch-field "from"))))
;;       (message "From is `%s', setting `smtpmail-smtp-server' to `%s'"
;;                from
;;                (cond
;;                 ((string-match "poppyer" from)
;; 		 (message "Using gmail")
;; 		 (setq user-mail-address "poppyer@gmail.com"
;; 		       user-full-name "poppyer")
;; ;; 		 (setq smtpmail-starttls-credentials '(("smtp.gmail.com" 587 nil nil))
;; ;; 		       smtpmail-auth-credentials '(("smtp.gmail.com" 587 "poppyer@gmail.com" nil))
;; ;; 		       smtpmail-default-smtp-server "smtp.gmail.com"
;; ;; 		       smtpmail-smtp-server "smtp.gmail.com"
;; ;; 		       smtpmail-smtp-service 587
;; ;; 		       )
;; 		 )
;; 		(t
;; 		 (setq user-full-name "Gaofeng Huang"
;; 		       user-mail-address "Gaofeng.Huang@balliol.ox.ac.uk")
;; 		)
;; 		)
;; 	       )
;;       )))

;; (add-hook 'message-send-hook 'fs-change-smtp)


  ;; The following two aren't completely necessary but may help.
  ;(setq smtpmail-local-domain "666.com")
  ;(setq smtpmail-sendto-domain "666.com")
  ;; If your SMTP server requires a username/password to authenticate, as
  ;; many do nowadays, set them like this:
  ;(setq smtpmail-auth-credentials  ; or use ~/.authinfo
;;'(("smtp.myserver.myisp.com" 25 "USER@SOMEWHERE" "PASSWORD")))

  ;; Other possibilities for getting smtpmail to work:
  ;;
  ;;  If for some reason you need to authenticate using the STARTTLS protocol
  ;;   (don't look into this unless you know what it is), use
  ;;  (setq smtpmail-starttls-credentials
  ;;      '(("YOUR SMTP HOST" 25 "~/.my_smtp_tls.key" "~/.my_smtp_tls.cert")))
  ;;  Requires external program
  ;;    ftp://ftp.opaopa.org/pub/elisp/starttls-*.tar.gz.
  ;;  See http://www.ietf.org/rfc/rfc2246.txt,
  ;;      http://www.ietf.org/rfc/rfc2487.txt
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;nntp news group;;;;;;;;;;;;;;
;;(add-to-list 'gnus-secondary-select-methods 
	     ;;      '(nntp "news.cn99.com"))	;; no posting allowed
;;	     '(nntp "freenews.netfront.net"))	;; to slow

;;  (defun set-timeout-5()
;;    (setq nntp-connection-timeout nil)
;;    )
;; ;; (add-hook 'nntp-server-opened-hook 'set-timeout-5)	;; seems no use, even the hook it self will be reset???
;; ;; (add-hook 'gnus-after-getting-new-news-hook 'set-timeout-5)
;; ;;(add-hook 'gnus-after-getting-new-news-hook 'nntp-close-server)
;; ;;(add-hook 'gnus-started-hook 'nntp-close-server)

(set 'gnus-secondary-select-methods 
	     '(
		   (nntp "freenews.netfront.net")		;; missing cn.bbs.comp.emacs
		   (nntp "news.cnusenet.org")			;; sync very slow
;; 		   (nntp "news.mixmin.net")				;; no posting
		   ))
;; (add-to-list 'gnus-secondary-select-methods 	     '(nntp ))



;; (add-to-list 'gnus-secondary-select-methods 
;; ;;	     '(nntp "freenews.netfront.net"
;; ;;	     '(nntp "news.yakoo.com"
;; 	     '(nntp "happynet"
;; 		    (nntp-address "news.happynet.org")
;; 		    (nntp-via-user-name "poppyer")
;; 		    (nntp-via-address "fedora.unix-center.net")
;; 		    (nntp-via-rlogin-command "ssh")
;; 		    (nntp-end-of-line "\n")
;; 		    (nntp-via-rlogin-command-switches ("-C" "-t" "-e" "none"))
;; 		    (nntp-open-connection-function nntp-open-via-rlogin-and-telnet)
;; 		    ))

;;freenews.maxbaud.net can search for groups
;; use ^ (server) to list a server


;; no need, put it in ~/News/all.SCORE
;;(setq gnus-global-score-files '("~/all.SCORE"))


;; show To rather than From when it is from myself
(setq gnus-extra-headers '(To Newsgroups X-Oxmail-Spam-Level))
(setq nnmail-extra-headers gnus-extra-headers)
(setq gnus-ignored-from-addresses "poppyer\\|Gaofeng")

;; show its encoding such that use 1 g to manually set encoding
(require 'gnus-art)
(add-hook 'gnus-startup-hook
          '(lambda ()
             (setq gnus-visible-headers
                   (concat "^User-Agent:\\|^Content-Type:\\|"
                           "^X-mailer:\\|^X-Newsreader:\\|^X-Sender:\\|"
			   "^Disposition-Notification-To:\\|"
                           gnus-visible-headers))))


;; encoding related
;;(setq gnus-default-charset 'gb2312)		;; each group has its own default
(setq gnus-group-charset-alist 
      '(("^hk\\>\\|^tw\\>\\|\\<big5\\>" big5)
       ("^cn\\>\\|\\<chinese\\>" gb2312)
       ("^fj\\>\\|^japan\\>" iso-2022-jp-2)
       ("^tnn\\>\\|^pin\\>\\|^sci.lang.japan" iso-2022-7bit)
       ("^relcom\\>" koi8-r)
       ("^fido7\\>" koi8-r)
       ("^\\(cz\\|hun\\|pl\\|sk\\|hr\\)\\>" iso-8859-2)
       ("^israel\\>" iso-8859-1)
       ("^han\\>" euc-kr)
       ("^alt.chinese.text.big5\\>" chinese-big5)
       ("^soc.culture.vietnamese\\>" vietnamese-viqr)
       ("^\\(comp\\|rec\\|alt\\|sci\\|soc\\|news\\|gnu\\|bofh\\)\\>" iso-8859-1)
       (".*" gb2312)))

;;(setq gnus-default-posting-charset 'utf-8)
(setq gnus-newsgroup-ignored-charsets
      '(unknown-8bit x-unknown x-gbk gb18030 gbk big5))  ;; ignore big5 for cn.bbs.soc.taiwan
;; send mail encoding priorities; alias not work, use decscribe-coding-system to check
(setq mm-coding-system-priorities '(iso-8859-1  chinese-iso-8bit chinese-big5 utf-8))
(setq gnus-summary-show-article-charset-alist
		'((1 . gb2312)
		  (2 . big5)
		  (3 . gbk))
;;           '((1 . utf-8)
;;            (2 . big5)
;;            (3 . gbk)
;;            (4 . utf-7))
)


(defalias 'mail-header-encode-parameter 'rfc2047-encode-parameter)     ;; for chinese name attachment
