;;(setq erc-nick "poppyer")		;; seems no use
;; (erc-select :server "irc.freenode.net" :port 6667 
;;		:nick "davidmccabe" :full-name "David McCabe" :password "xxx")

;;(setq erc-hide-list '("JOIN" "PART" "QUIT" "MODE"))
(setq erc-hide-list '("MODE"))
(setq erc-header-line-format nil)
(setq erc-fill-column 98)

(require 'erc-track)
(setq erc-track-exclude-types '("NICK" "JOIN" "PART" "QUIT"))

(define-key erc-mode-map (kbd "C-c C-c") 'erc-track-switch-buffer)  ;; default erc-toggle-interpret-controls


(require 'erc-nick-colors)

(defun my-quit-color-dehighlight ()
  (save-excursion
    (goto-char (point-min))
    (if (looking-at "\\*\\*\\*.*has \\(quit\\|left\\)")
		(put-text-property (point-min) (point-max)
						   'face (list :foreground "gray")
						   )
      )))

;; This adds the ERC message insert hook.                                                                                        
(add-hook 'erc-insert-modify-hook 'my-quit-color-dehighlight)


(defun my-name-changed-formatter ()
  (save-excursion
    (goto-char (point-min))
	(when (looking-at ".*`\\(.*\\)' changed name from `\\(.*\\)' to `\\(.*\\)'")
	  (goto-char (1- (match-beginning 3)))
	  (insert "\n      ")
	  (goto-char (1- (match-beginning 2)))
	  (insert "\n      ")
	  )
	))
(add-hook 'erc-insert-modify-hook 'my-name-changed-formatter)


(setq bitlbee-filter-name-changed '( "fund0800" "poppyer2001"))

(defun my-name-changed-filter (s)
  (when (string-match "`\\(.*\\)' changed name from `\\(.*\\)' to `\\(.*\\)'" s)
	(let ((user (substring s (match-beginning 1) (match-end 1)))
		  (from (substring s (match-beginning 2) (match-end 2)))
		  (to (substring s (match-beginning 3) (match-end 3))))
	  (dolist (filtered-user bitlbee-filter-name-changed)
		(when (string= user filtered-user)
		  (setq erc-insert-this nil)
		  (return)
		  )
		)
	  )
	)
  )
(add-hook 'erc-insert-pre-hook 'my-name-changed-filter)


(require 'erc-speedbar)
(setq erc-speedbar-sort-users-type 'alphabetical)


(require 'erc-join)
(erc-autojoin-mode t)
(setq erc-autojoin-channels-alist
      '( ("freenode.net" "#og-zh ogm185286")
         ))


;; (add-to-list 'erc-modules 'scrolltobottom)
;; (setq erc-input-line-position -2)

;; my logging for bitlbee only (localhost)
(add-to-list 'erc-modules 'log)
(setq erc-log-file-coding-system 'utf-8)
(setq erc-log-write-after-insert t)
(setq erc-save-buffer-on-part nil)
(setq-default erc-enable-logging '(lambda(buffer)
				    (and
				     (erc-log-all-but-server-buffers buffer)
				     (string= "localhost" erc-session-server)
				     (not (string= "&bitlbee" (buffer-name buffer)))
				     )
			    )
      )

;; logging:
;;(require 'erc-log)
;;(setq erc-log-insert-log-on-open nil)		;;default
;;(setq erc-log-channels t)			;;unknown???
;;(setq erc-log-channels-directory "~/.irclogs/")
;;(setq erc-save-buffer-on-part t)		;;default
;;(setq erc-hide-timestamps nil)		;;default

;;(defadvice save-buffers-kill-emacs (before save-logs (arg) activate)
;;  (save-some-buffers t (lambda () (when (and (eq major-mode 'erc-mode)
;;                                             (not (null buffer-file-name)))))))

;;(add-hook 'erc-insert-post-hook 'erc-save-buffer-in-logs)
;;(add-hook 'erc-mode-hook '(lambda () (when (not (featurep 'xemacs))
;;                                       (set (make-variable-buffer-local
;;                                             'coding-system-for-write)
;;                                            'emacs-mule))))

;; end logging
